<?php 

if(! function_exists('encode')) {
	
	function encode($str = '') {
		$str = base64_encode($str);
		$result = base64_encode($str);
		
		return $result;
	}
	
}

if(! function_exists('decode')) {
	
	function decode($str = '') {
		$str = base64_decode($str);
		$result = base64_decode($str);
		
		return $result;
	}
	
}

if( !function_exists('gen_paging')) {
	function gen_paging($page_data = array()) {
		$page_result = '';
		
		$func_name = 'pageLoad';

		$count = 1;
		
		if(isset($page_data['load_func_name'])) {
			if($page_data['load_func_name'])
				$func_name = $page_data['load_func_name'];
		}
		
		if($page_data['count_row'] > 1)
			$count = ceil($page_data['count_row']/$page_data['limit']);
		
		$page_result .= '<ul class="pagination pagination-md no-margin pull-right">
							<li ' . ( $page_data['current'] == 1 ? 'class="active"' : '' ) . '><a href="javascript:void(0)" ' . ($page_data['current'] == 1 ? '' : 'onclick = "'.$func_name.'(1)"') .' >&laquo; First</a></li>';
		
		$paging_show 	= 3;
		$page_start		= $page_data['current'] - $paging_show;
		$page_start		= $page_start < 1 ? 1 : $page_start;
		
		$page_end		= $page_data['current'] + $paging_show;
		$page_end		= $count > $page_end ? $page_end : $count;
		$page_end		= $count > 1 ? $page_end : 1;
		
		if($page_start > 1)
			$page_result .= '<li class="active"><a href="javascript:void(0)">...</a></li>';
		
		for($i=$page_start ; $i<=$page_end; $i++) {
			$page_result .= '<li '.($page_data['current'] == $i ? 'class="active"' : '').'><a href="javascript:void(0)" '.($page_data['current'] == $i ? "" : 'onclick="'.$func_name.'('.$i.')"').'>'.$i.'</a></li>';
			
		}
		
		if($count > $page_end)
			$page_result .= '<li class="active"><a href="javascript:void(0)">...</a></li>';
		
		$page_result .= '<li ' . ( $page_data['current'] == $page_end ? 'class="active"' : '' ) . '><a href="javascript:void(0)" onclick = "'.$func_name.'('.$count.')">Last &raquo;</a></li>';
		$page_result .='</ul>';
		
		return $page_result;
	}
}

if ( ! function_exists('tgl_indo'))
{
	function tgl_indo($tgl)
	{
		$ubah = gmdate($tgl, time()+60*60*8);
		$pecah = explode("-",$ubah);
		$tanggal = $pecah[2];
		$bulan = bulan($pecah[1]);
		$tahun = $pecah[0];
		return $tanggal.'-'.$bulan.'-'.$tahun;
	}
}

if ( ! function_exists('bulan'))
{
	function bulan($bln)
	{
		switch ($bln)
		{
			case 1:
				return "Jan";
				break;
			case 2:
				return "Feb";
				break;
			case 3:
				return "Mar";
				break;
			case 4:
				return "Apr";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Jun";
				break;
			case 7:
				return "Jul";
				break;
			case 8:
				return "Agust";
				break;
			case 9:
				return "Sep";
				break;
			case 10:
				return "Okt";
				break;
			case 11:
				return "Nov";
				break;
			case 12:
				return "Des";
				break;
		}
	}
}

if ( ! function_exists('nama_hari'))
{
	function nama_hari($tanggal)
	{
		$ubah = gmdate($tanggal, time()+60*60*8);
		$pecah = explode("-",$ubah);
		$tgl = $pecah[2];
		$bln = $pecah[1];
		$thn = $pecah[0];

		$nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
		$nama_hari = "";
		if($nama=="Sunday") {$nama_hari="Minggu";}
		else if($nama=="Monday") {$nama_hari="Senin";}
		else if($nama=="Tuesday") {$nama_hari="Selasa";}
		else if($nama=="Wednesday") {$nama_hari="Rabu";}
		else if($nama=="Thursday") {$nama_hari="Kamis";}
		else if($nama=="Friday") {$nama_hari="Jumat";}
		else if($nama=="Saturday") {$nama_hari="Sabtu";}
		return $nama_hari;
	}
}

if ( ! function_exists('hitung_mundur'))
{
	function hitung_mundur($wkt)
	{
		$waktu=array(	365*24*60*60	=> "tahun",
						30*24*60*60		=> "bulan",
						7*24*60*60		=> "minggu",
						24*60*60		=> "hari",
						60*60			=> "jam",
						60				=> "menit",
						1				=> "detik");

		$hitung = strtotime(gmdate ("Y-m-d H:i:s", time () +60 * 60 * 8))-$wkt;
		$hasil = array();
		if($hitung<5)
		{
			$hasil = 'kurang dari 5 detik yang lalu';
		}
		else
		{
			$stop = 0;
			foreach($waktu as $periode => $satuan)
			{
				if($stop>=6 || ($stop>0 && $periode<60)) break;
				$bagi = floor($hitung/$periode);
				if($bagi > 0)
				{
					$hasil[] = $bagi.' '.$satuan;
					$hitung -= $bagi*$periode;
					$stop++;
				}
				else if($stop>0) $stop++;
			}
			$hasil=implode(' ',$hasil).' yang lalu';
		}
		return $hasil;
	}
}

if(! function_exists('get_kategori')) {
	
	function get_kategori($id="") {
		
		$CI =& get_instance();
		$CI->load->model('m_kategori');
		
		$query = $CI->m_kategori->getSelect($status='', $key='', $limit='', $offset='', $where='');
		
		foreach($query->result() as $row){ ?>
			
			<option value="<?php echo $row->ID?>" <?php echo $row->ID == $id ? 'selected' : '' ?>><?php echo $row->NAMA_KTGR?></option>
		<?php }
	}
}

if(! function_exists('get_menu_kategori')) {
	
	function get_menu_kategori($id="") {
		
		$CI =& get_instance();
		$CI->load->model('m_kategori');
		
		$query = $CI->m_kategori->getSelect($status='1', $key='', $limit='', $offset='', $where='');
		
		foreach($query->result() as $row){ ?>
			
			<li><a href="<?php echo site_url('artikel/categori/'.$row->ID.'/'.$row->URL)?>"><?php echo $row->NAMA_KTGR;?></a></li>
		<?php }
	}
}

