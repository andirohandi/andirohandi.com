<style>
#title-page{
	background-image:url('<?php echo HTTP_F?>img/banner-title.png');
	background-repeat:no-repeat;
	min-height:50px;
	margin-left:-38px;
}
#title-page p{
	color:white;
	font-weight:bold;
	margin-left:20px;
	padding-top:3px;
}
.h1 small {
	font-size: 24px;
}
</style>

<div class="container">
	<br/>
	<!-- Tengah -->
	<div class="col-md-8">
		
			<div class="row" >
				<div class="col-md-12" >
				<div class="col-md-12 well" style="border-right:none;">
					<div id="title-page"><p>Profile</p></div>
					<div class="row">
						<div class="container">
							<div class="row">
								<div class="col-md-3">
									<center><img src="<?php echo base_url()?>assets/img/no_image.png" class="img img-responsive img-thumbnail">
									<br/><br/><small>Maaf, poto belum ada sedang dalam proses pencarian !</small><br/><br/></center>
								</div>
								<div class="col-md-9">
									<div class="row">
										<div class="col-md-12">
											<h3 style="margin-top:0px;color:#772953;font-weight:bold;">Andi Rohandi</h3>
											<label>Cibaligo Timur RT 04 RW 29, Kel. Cibeureum, Kec. Cimahi Selatan, Cimahi 40535</label>
											<br/><br/>
											<table>
												<tr  height="50px">
													<td valign="top">Universitas </td><td width="20px" align="center" valign="top"> : </td><td valign="top"><b><a href="http://iwu.ac.id/" target="_blank">International Women University</a></b> (Bandung)<br/>- Saat ini sedang dalam proses skripsi</td>
												</tr>
												<tr height="50px">
													<td valign="top">Pekerjaan </td><td width="20px" align="center" valign="top"> : </td><td valign="top">Freelancer dan seorang staff ppds di sebuah perusahaan swasta di kawasan industri Cimahi</td>
												</tr>
												<tr height="30px">
													<td valign="top"><span> Whatsapp </span></td><td width="20px" align="center" valign="top"> : </td><td valign="top">0896-9042-8088</td>
												</tr>
												<tr height="30px">
													<td valign="top"><span> Email </span></td><td width="20px" align="center" valign="top"> : </td><td valign="top">andirohandi.android@gmail.com</td>
												</tr>
											</table>
										</div>
										<br/><br/>
										<div class="col-md-12">
											<label>Sosial Media</label>
										</div>
										<div class="col-md-12">
											<a href="https://www.facebook.com/secangkirairdigurunpasir" class="btn btn-info" style="font-weight:bold" target="_blank"><i class="fa fa-facebook"></i> Facebook</a>
											<a href="https://twitter.com/andicybers" class="btn btn-info" style="font-weight:bold" target="_blank"><i class="fa fa-twitter"></i> Twitter</a>
											<a href="https://bitbucket.org/andirohandi/" class="btn btn-info" style="font-weight:bold" target="_blank"><i class="fa fa-google-plus"></i> Bitbucket</a>
											<a href="https://github.com/Andirohandi" class="btn btn-info" style="font-weight:bold" target="_blank"><i class="fa fa-google-plus"></i> Github</a>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>