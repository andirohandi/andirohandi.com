<style>
#title-page{
	background-image:url('<?php echo HTTP_F?>img/banner-title.png');
	background-repeat:no-repeat;
	min-height:50px;
	margin-left:-38px;
}
#title-page p{
	color:white;
	font-weight:bold;
	margin-left:20px;
	padding-top:3px;
}
</style>
<div class="container">
	<br/>
	<!-- Tengah -->
	<div class="col-md-8">
		<div class="row" >
			<div class="col-md-12" >
				<div class="col-md-12 well" style="border-right:none;">
					<div id="title-page"><p>Testimoni</p></div>
					
					<div id="dtTestimoni"></div>
					
				</div>
				<div class="col-md-12 well" style="border-right:none;">
					<div id="title-page"><p>Kirim Testimoni</p></div>
					<?php echo form_open('testimoni/create');?>
						<div class="row">
							<div class="col-md-12">
								<?php if($this->session->flashdata('result')==2){ ?>
									<div class='alert alert-danger'>
										<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
										<?php echo validation_errors()?>
									</div>
								<?php }else if($this->session->flashdata('result')==3){ ?> 
									<div class='alert alert-danger'>
										<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
										<i class="fa fa-remove"></i> Testimoni gagal dikirim. Silahkan untuk mencoba lagi
									</div>
								<?php }else if($this->session->flashdata('result')==1){ ?> 
									<div class='alert alert-success'>
										<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
										<i class="fa fa-check"></i> Testimoni berhasil dikirim
									</div>
								<?php } ?>
								<div class="form-group">
									<label for="name">
										Nama</label>
									<input type="text" value="<?php echo set_value('nama')?>" class="form-control" id="nama" name="nama" placeholder="Nama.." required="required" />
								</div>
								<div class="form-group">
									<label for="email">
										Email</label>
									<div class="input-group">
										<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
										</span>
										<input type="email" class="form-control" id="email" name="email" placeholder="Email.." required="required" value="<?php echo set_value('email')?>" /></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="name">
										Testimoni</label>
									<textarea name="testimoni" id="testimoni" class="form-control" rows="9" cols="25" required="required"
										placeholder="Testimoni.." value="<?php echo set_value('testimoni')?>" ></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info pull-right" id="kirim_testimoni" name="kirim_testimoni" >
									Kirim Testimoni</button>
							</div>
						</div>
					<?php echo form_close()?>
				</div>
			</div>
		</div>

<script>
$(document).ready(function(){
	
	pageLoad(1);

});

function pageLoad(page)
{
	
	var limit 	= 5;
	var key 	= '';
	var status 	= '';
	
	$.ajax({
		url		: '<?php echo site_url()?>testimoni/read/'+page,
		type	: 'POST',
		dataType: 'html',
		data	: {limit:limit,key:key,status:status},
		beforeSend : function(){
			
		},
		success : function(result)
		{
			$('#dtTestimoni').empty().append(result);
		}
	});
}
</script>