<?php 
	$no = ($paging['limit']*$paging['current'])-$paging['limit'];
	$no++;
	if($list->num_rows() > 0) { 
		foreach($list->result() as $row) { 
			$tgl = explode(" ",$row->TANGGAL);
			$tgl = $tgl[0];	?>
			<div class="row">
				<div class="col-md-2">
					<img src="<?php echo base_url() ?>assets/img/no_image.png" class="img img-responsive img-circle pull-right" style="width:64px;border:1px solid grey"/>
				</div>
				<div class="col-md-10">
					<div class="panel panel-default panel-comment">
						<div class="panel-heading">
							<strong style="cursor:pointer; color:black;"> <?php echo $row->NAMA?></strong><br>
							<small style="margin-top:15px"> <i class="fa fa-calendar"></i>  <?php echo tgl_indo($tgl)?><br/><br/>
							<?php echo $row->TESTIMONI?> &nbsp;&nbsp;&nbsp;</small>
						</div>
					</div>
				</div>
			</div>
		<?php $no++; } ?>
		<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
		<div class='row'>
			<div class='col-sm-4 col-xs-12' style='margin-top:5px;margin-bottom:10px'>
				
			</div>
			<div class='col-sm-8 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:10px'>
				<?php echo $paging['list'] ?>
			</div>
		</div>
	<?php } else{ ?>
		<div style='height:400px'><h3 style='margin-left:15px'>Testiomni belum ada</h3></div>
	<?php }
?>

	