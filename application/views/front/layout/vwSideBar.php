	<div class="col-md-4 sol-sm-4">
		<div class="row">
			<div class="col-md-12" style="padding-right:0px;">
				<div class="panel panel-default">	
					<div class="panel-heading" style="font-size:20px;color:#fff;background-color:#DD4814;border-bottom:5px solid #E18300"><i class="fa fa-bookmark"></i> Postingan Terbaru</div>
					<div class="panel-body" style="min-height:400px;">
						
						<?php $new_post = $this->m_post->getSelect($where='', $limit='5', $offset='0', $kategori='');
							foreach($new_post->result() as $row){ ?>
								<div class="panel panel-default panel-comment">
									<div class="panel-heading">
										<a href="<?php echo site_url('artikel/detail/'.encode($row->ID)."/".$row->URL)?>"><strong style="cursor:pointer; color:black;" onclick=""><?php echo $row->JUDUL?></strong></a><br>
										<small style="margin-top:15px"><i class="fa fa-user"></i> Andi Rohandi &nbsp;&nbsp;&nbsp;<i class="fa fa-calendar"></i>  <?php echo tgl_indo($row->TGL_INPUT)?><br/></small>
									</div>
								</div>
							<?php }
						?>
					</div>
				</div>
			</div>
			<div class="col-md-12" style="padding-right:0px;">
				<div class="panel panel-default">	
					<div class="panel-heading" style="font-size:20px;color:#fff;background-color:#DD4814;border-bottom:5px solid #E18300"><i class="fa fa-cogs"></i> Layanan</div>
					<div class="panel-body" style="min-height:250px;">
						<div class="panel panel-default panel-comment">
							<div class="panel-heading">
								<center><strong style="cursor:pointer; color:black;">Jasa Pembuatan Aplikasi dan atau Sistem Informasi Berbasis Website</strong></center><br>
								<small style="margin-top:15px"><strong><a href="<?php echo site_url('')?>">andirohandi.com</a></strong>, menerima jasa pembuatan aplikasi dan ataupun sistem informasi berbasis website.<br/><br/>
								
								Untuk info lebih jelas silahkan hubungi kami di :<br/>
								- WA &nbsp;&nbsp;: 0896-9042-8088<br/>
								- Email : andirohandi.android@gmail.com<br/>
								- Atau klik link berikut <label class="label label-primary"><a href="<?php echo site_url('kontak')?>">Detail Kontak <i class="fa fa-long-arrow-right"></i></a><label>
								</small>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12" style="padding-right:0px;">
				 <div class="panel panel-default">	
					<div class="panel-heading" style="font-size:20px;color:#fff;background-color:#DD4814;border-bottom:5px solid #E18300"><i class="fa fa-envelope"></i> Kotak Saran</div>
					<?php if($this->session->flashdata('result')==5){ ?>
						<div class='alert alert-danger'>
							<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
							<?php echo validation_errors()?>
						</div>
					<?php }else if($this->session->flashdata('result')==6){ ?> 
						<div class='alert alert-danger'>
							<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
							<i class="fa fa-remove"></i> Saran gagal dikirim. Silahkan untuk mencoba lagi
						</div>
					<?php }else if($this->session->flashdata('result')==4){ ?> 
						<div class='alert alert-success'>
							<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
							<i class="fa fa-check"></i> Saran berhasil dikirim
						</div>
					<?php } ?>
					<div class="panel-footer text-right" style="background-color:white;">
						<div class="row">
							<?php echo form_open('kontak/create_saran') ?>
								<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px;">
									<input type="email" name="email_komentar" id="email_komentar" class="form-control" style="border-radius:5px;" placeholder="Masukkan Email" required=""><br>
									<textarea rows="4" class="form-control" name="isi_komentar" id="isi_komentar" placeholder="Pesan" required=""></textarea>
								</div>
								<input type="hidden" name="url" id="url" class="form-control" style="border-radius:5px;" value="<?php  echo $this->uri->segment(1);  ?>" required=""><br>
								<div class="col-md-5 col-sm-5 col-xs-12 pull-right">
									<input type="submit" class="btn btn-info" value="Kirim" name="kirim_saran" id="kirim_saran">
								</div>
							<?php echo form_close();?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- /.Tengah -->