<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="andirohandi, andi rohandi, portofolio, free aplikasi, andi, rohandi">
        <meta name="description" content="Andirohandi.com">
        <meta name="author" content="Andi Rohandi">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/front/img/logo.ico">

        <title><?php echo $title; ?></title>

        <link href="<?php echo HTTP_F ?>bootstrap/css/bootstrap-custom.css" rel="stylesheet">
		
		<link href="<?php echo HTTP_F ?>css/custom.css" rel="stylesheet">
		<link href="<?php echo HTTP_F ?>css/mystyle.css" rel="stylesheet">
		<link href="<?php echo HTTP_F ?>font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet">

        <script src="<?php echo HTTP_F ?>js/jquery.min.js"></script>
        <script src="<?php echo HTTP_F ?>bootstrap/js/bootstrap.min.js"></script>
		
		<script>
            $(document).ready(function () {
                $(".searchinput").keyup(function () {
                    $(this).next().toggle(Boolean($(this).val()));
                });
                $(".searchclear").toggle(Boolean($(".searchinput").val()));
                $(".searchclear").click(function () {
                    $(this).prev().val('').focus();
                    $(this).hide();
                });
            });
        </script>

        <style>
			#toTop{
				position: fixed;
				bottom: 10px;
				right: 10px;
				cursor: pointer;
				display: none;
			}
			#menu:hover{
				background:yellow;
			}
            .searchinput {
                width: 200px;
            }

            .searchclear {
                position:absolute !important;
                right:5px !important;
                top:0 !important;
                bottom:0 !important;
                height:14px !important;
                margin:auto !important;
                font-size:14px !important;
                cursor:pointer !important;
                color:#ccc !important;
            }

            /* enable absolute positioning */
            .inner-addon { 
                position: relative; 
            }

            /* style icon */
            .inner-addon .glyphicon-search {
                position: absolute;
                padding: 10px;
                pointer-events: none;
            }

            /* align icon */
            .left-addon .glyphicon-searcb  { left:  0px;}
            .right-addon .glyphicon-searcb { right: 0px;}

            /* add padding  */
            .left-addon input  { padding-left:  30px; }
            .right-addon input { padding-right: 30px; }
			
			ul li{
				margin-top:5px;
			}
			.actv{
				background-color:#E18300;border-radius:10px 10px 0 0;margin-top:5px;
			}
			
			@media (max-width:400px){
				.actv{
					border-radius:0 0 0 0;margin-top:none;
				}
			}
			
			.content-mid a{
				color: #1474CA !important;
			}

			#amazingslider-3{
				height: auto !important;
				max-height:300px !important;
			}

			.carousel {
				margin-bottom: 0;
				padding: 0 40px 30px 40px;
			}
			/* The controlsy */
			.carousel-control {
				left: -12px;
				height: 40px;
				width: 40px;
				background: none repeat scroll 0 0 #222222;
				border: 4px solid #FFFFFF;
				border-radius: 23px 23px 23px 23px;
				margin-top: 90px;
			}
			.carousel-control.right {
				right: -12px;
			}
			/* The indicators */
			.carousel-indicators {
				right: 50%;
				top: auto;
				bottom: -10px;
				margin-right: -19px;
			}
			/* The colour of the indicators */
			.carousel-indicators li {
				background: #cecece;
			}
			.carousel-indicators .active {
				background: #428bca;
			}

			/*carousel lama*/
			/*Makes images fully responsive 

			.img-responsive,
			.thumbnail > img,
			.thumbnail a > img,
			.carousel-inner > .item > img,
			.carousel-inner > .item > a > img {
				display: block;
				width: 100%;
				height: auto;
			}

			 ------------------- Carousel Styling ------------------- 

			.carousel-inner {
				border-radius: 15px;
			}

			.carousel-caption {
				background-color: rgba(0,0,0,.5);
				position: absolute;
				left: 0;
				right: 0;
				bottom: 0;
				z-index: 10;
				padding: 0 0 10px 25px;
				color: #fff;
				text-align: left;
			}

			.carousel-indicators {
				position: absolute;
				bottom: 0;
				right: 0;
				left: 0;
				width: 100%;
				z-index: 15;
				margin: 0;
				padding: 0 25px 25px 0;
				text-align: right;
			}

			.carousel-control.left,
			.carousel-control.right {
				background-image: none;
			}


			 ------------------- Section Styling - Not needed for carousel styling ------------------- 

			.section-white {
				padding: 10px 0;
			}

			.section-white {
				background-color: #fff;
				color: #555;
			}

			.carousel .item {
				height: 250px !important;
				background-color: #777;
			}
			.carousel{
				margin-top:0px !important;
			}
			.carousel-caption{
				background-color: #777777;
			}*/
			/*carousel lama*/


			//comment panel
			.thumbnail {
				padding:0px;
			}
			.panel-comment {
				position:relative;
			}
			.panel-comment > .panel-heading{
				background-color:#E6E2E2 !important;
			}
			.panel-comment>.panel-heading:after,.panel>.panel-heading:before{
				position:absolute;
				top:11px;left:-16px;
				right:100%;
				width:0;
				height:0;
				display:block;
				content:" ";
				border-color:transparent;
				border-style:solid solid outset;
				pointer-events:none;
			}
			.panel-comment>.panel-heading:after{
				border-width:7px;
				border-right-color:#E6E2E2;
				margin-top:1px;
				margin-left:2px;
			}
			.panel-comment>.panel-heading:before{
				border-right-color:#ddd;
				border-width:8px;
			}
        </style>
    </head>
    <body style="background-color:#f5f5f5;">
       <div class="container" style="margin-top:8px;">
            <div class="page-header">
                <div class="row hidden-xs hidden-sm">
                    <div class="col-md-12" style="padding-bottom:2px;">
                        <img src="<?php echo base_url(); ?>assets/front/img/banner.png" class="img-responsive"/>
                    </div>
                </div>
				<!-- BIRU TUA 3a5795 --!-->
                <div class="navbar navbar-default" style="margin-bottom: 0px;background:#dd4814;border-bottom:10px solid #E18300">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <i class="fa fa-bars"></i> Menu
                            </button>
                            <a class="navbar-brand" href="<?php echo site_url('')?>" style="padding-top:20px"><b>Ar.com</b><b class="visible-md visible-lg pull-right" style="padding-left:30px;">|</b></a>
                        </div>
                        <div class="navbar-collapse collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                                <li class="<?php echo $pg == 'beranda' ? 'actv' : '' ?>" id="menu"><a href="<?php echo site_url('beranda') ?>">Beranda</a></li>
								<li class="<?php echo $pg == 'article' ? 'actv' : '' ?>" id="menu" >
									<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Artikel <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
									   <?php echo get_menu_kategori();?>
                                    </ul>
								</li>
                                <li class="<?php echo $pg == 'aplikasi' ? 'actv' : '' ?> hide" id="menu"><a href="<?php echo site_url('aplikasi') ?>">Aplikasi</a></li>
                                <li class="<?php echo $pg == 'portofolio' ? 'actv' : '' ?>" id="menu"><a href="<?php echo site_url('portofolio'); ?>">Portofolio</a></li>
                                <li class="<?php echo $pg == 'profile' ? 'actv' : '' ?> " id="menu"><a href="<?php echo site_url('profile'); ?>">Profile</a></li>
                                <li class="<?php echo $pg == 'testimoni' ? 'actv' : '' ?>" id="menu"><a href="<?php echo site_url('testimoni'); ?>">Testimoni</a></li>
                                <li class="<?php echo $pg == 'kontak' ? 'actv' : '' ?>" id="menu"><a href="<?php echo site_url('kontak'); ?>">Kontak</a></li>
                            </ul>
                            <div class="navbar-form navbar-right">
                                <div class="input-group inner-addon left-addon" style="width:100%;padding-top:3px">
                                    <i class="glyphicon glyphicon-search" style="padding-top:12px"></i>
                                    <input id="searchinput" type="text" class="form-control searchinput" style="border-radius:20px;width:100%" placeholder="Search" value="" onchange="pageLoad(1)" >
                                    <span id="searchclear" class="searchclear glyphicon glyphicon-remove-circle"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>