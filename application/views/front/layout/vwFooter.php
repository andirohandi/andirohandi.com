		<div class="container">
			<footer id="footer" class="vspace20 " style="margin-bottom:-700px;color:#fff;background-color:#DD4814">
				<div class="row">
					<div class="col-md-12" style="font-size:12px; vertical-align: middle;">
						<br>
						<center><p>Andi Rohandi | <a href="<?php echo site_url('')?>">andirohandi.com</a></p></center>
					</div>
				</div>
				<p></p>
			</footer>
		</div>
		<script>
			$(document).ready(function(){
				 $('body').append('<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
					$(window).scroll(function () {
						if ($(this).scrollTop() != 0) {
							$('#toTop').fadeIn();
						} else {
							$('#toTop').fadeOut();
						}
					}); 
				$('#toTop').click(function(){
					$("html, body").animate({ scrollTop: 0 }, 600);
					return false;
				});
			});
		</script>
	</body>
</html>
