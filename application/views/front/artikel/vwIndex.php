<style>
#title-page{
	background-image:url('<?php echo HTTP_F?>img/banner-title.png');
	background-repeat:no-repeat;
	min-height:50px;
	margin-left:-38px;
}
#title-page p{
	color:white;
	font-weight:bold;
	margin-left:20px;
	padding-top:3px;
}
</style>
<div class="container">
	<br/>
	<!-- Tengah -->
	<div class="col-md-8">
		<div class="row" >
			<div class="col-md-12" >
				<div class="col-md-12 well" style="border-right:none;">
					<div id="title-page"><p>Artikel | <?php echo $artikel['NAMA_KTGR']?></p></div>
					
					<div id="dtArtikel"></div>
					
				</div>
			</div>
		</div>

<script>
$(document).ready(function(){
	
	pageLoad(1);

});

function pageLoad(page)
{
	
	var limit 	= 5;
	var key 	= $('#searchinput').val();
	var status 	= 1;
	var kategori= "<?php echo $artikel['ID'];?>";
	
	$.ajax({
		url		: '<?php echo site_url()?>artikel/categori_read/'+page,
		type	: 'POST',
		dataType: 'html',
		data	: {limit:limit,key:key,status:status,kategori:kategori},
		beforeSend : function(){
			
		},
		success : function(result)
		{
			$('#dtArtikel').empty().append(result);
		}
	});
}
</script>