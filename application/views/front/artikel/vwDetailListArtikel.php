<?php foreach($komen->result() as $row) {  
	$tgl = explode(" ",$row->TANGGAL);
	$tgl = $tgl[0];	?> 
	<div class="row">
		<div class="col-md-2">
			<img src="<?php echo base_url() ?>assets/img/no_image.png" class="img img-responsive img-circle pull-right" style="width:64px;border:1px solid grey"/>
		</div>
		<div class="col-md-10">
			<div class="panel panel-default panel-comment">
				<div class="panel-heading">
					<strong style="cursor:pointer; color:black;"> <?php echo $row->NAMA?></strong>
					<small style="margin-top:15px"> <span class="pull-right"><i class="fa fa-calendar"></i>  <?php echo tgl_indo($tgl)?></span><br/>
					<?php echo $row->KOMENTAR ?>&nbsp;&nbsp;&nbsp;</small>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
<div class='row'>
    <div class='col-sm-4 col-xs-12' style='margin-top:5px;margin-bottom:10px'>
        <br/>Total <?php echo $paging['count_row'] ?> Komentar
    </div>
    <div class='col-sm-8 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:10px'>
        <?php echo $paging['list'] ?>
    </div>
</div>