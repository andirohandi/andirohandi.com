<style>
#title-page{
	background-image:url('<?php echo HTTP_F?>img/banner-title.png');
	background-repeat:no-repeat;
	min-height:50px;
	margin-left:-38px;
}
#title-page p{
	color:white;
	font-weight:bold;
	margin-left:20px;
	padding-top:3px;
}
.radius {
	border-radius: 5px 5px 5px 5px;
	-moz-border-radius: 5px 5px 5px 5px;
	-webkit-border-radius: 5px 5px 5px 5px;
	border: 0px solid #e3e3e3;
}

.fb-share-button
{
	transform: scale(1.3);
	-ms-transform: scale(1.3);
	-webkit-transform: scale(1.3);
	-o-transform: scale(1.3);
	-moz-transform: scale(1.3);
	transform-origin: top left;
	-ms-transform-origin: top left;
	-webkit-transform-origin: top left;
	-moz-transform-origin: top left;
	-webkit-transform-origin: top left;
}

.twitter-share-button
{
	transform: scale(0.94);
	-ms-transform: scale(0.94);
	-webkit-transform: scale(0.94);
	-o-transform: scale(0.94);
	-moz-transform: scale(0.94);
	transform-origin: top left;
	-ms-transform-origin: top left;
	-webkit-transform-origin: top left;
	-moz-transform-origin: top left;
	-webkit-transform-origin: top left;
}


</style>
<?php 
$judul = $detail['JUDUL'];
$deskripsi = $detail['DESKRIPSI'];
$image	= $detail['IMAGE'] != "" ? $detail['IMAGE'] : 'assets/img/no_poto.png';
$url	= 'artikel/detail/'.encode($detail['ID']).'/'.$detail['URL'];
?>
<div class="container">
	<br/>
	<!-- Tengah -->
	<div class="col-md-8">
		<div class="row" >
			<div class="row" >
				<div class="col-md-12">
					<h2 style="color:#772953;font-family:callibri;margin-top:2px"><?php echo $detail['JUDUL']?></h2>
					<?php if($detail['IMAGE'] != ""){ ?><br/><center><img src="<?php echo site_url($detail['IMAGE'])?>" class="img img-responsive img-thumbnail"></center> <?php } ?>
					<br/>
					<?php echo $detail['DESKRIPSI'];?>
					<br/>
					<div class="row">
						<div class="col-md-8">
							<small><i class="fa fa-folder-open"></i> <label class="label label-info"><?php echo $detail['NAMA_KTGR']?></label> | <i class="fa fa-user"></i> Andi Rohandi | <i class="fa fa-calendar"></i>  <?php echo tgl_indo($detail['TGL_INPUT']);?> | <i class="fa fa-comments"></i> <?php echo $jmlah; ?> Komentar </small>
							<br/><br/>
						</div>
						<div class="col-md-4">
							<div class="col-md-12">
							<div class='pull-right'>
								<div id="fb-root"></div>
								<div style='margin-right:32%'>
									<div class="fb-share-button" data-size='large' 
										data-href="<?php echo site_url('artikel/detail/'.encode($detail['ID'])."/".$detail['URL']) ?>" 
										data-layout="button">
									</div>
								</div>
							</div>
							<div class='pull-right'>
								<div style='margin-right:7%'>
									<a href="https://twitter.com/share" class="twitter-share-button"{count} data-size='large' data-url="<?php echo site_url('artikel/detail/'.encode($detail['ID'])."/".$detail['URL']) ?>" data-text="<?php echo $detail['JUDUL'] ?>">Tweet</a>
								</div>
							</div>
							<div class='pull-right'>
								<div class="g-plus" data-height='28' data-action="share" data-annotation="none" data-href="<?php echo site_url('artikel/detail/'.encode($detail['ID'])."/".$detail['URL']) ?>">
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12" style="border-top:1px solid #e6e2e2;">
				<div class="col-md-12">
					<br/>
					<div id="title-page"><p><?php echo $jmlah ?> Komentar</p></div>
					
					<div id="dtKomentar"></div>
					
				</div>
			</div>
			<div class="col-md-12" style="border-right:none;">
				<div class="col-md-12">
					<div id="title-page"><p>Kirim Komentar</p></div>
					<?php echo form_open('artikel/creat_komentar');?>
						<div class="row">
							<div class="col-md-12">
								<?php if($this->session->flashdata('result')==2){ ?>
									<div class='alert alert-danger'>
										<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
										<?php echo validation_errors()?>
									</div>
								<?php }else if($this->session->flashdata('result')==3){ ?> 
									<div class='alert alert-danger'>
										<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
										<i class="fa fa-remove"></i> Komentar gagal dikirim. Silahkan untuk mencoba lagi
									</div>
								<?php }else if($this->session->flashdata('result')==1){ ?> 
									<div class='alert alert-success'>
										<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
										<i class="fa fa-check"></i> Komentar berhasil dikirim
									</div>
								<?php } ?>
								<div class="form-group">
									<label for="name">
										Nama</label>
									<input type="text" value="<?php echo set_value('nama')?>" class="form-control" id="nama" name="nama" placeholder="Nama.." required="required" />
								</div>
								<div class="form-group">
									<label for="email">
										Email</label>
									<div class="input-group">
										<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
										</span>
										<input type="email" class="form-control" id="email" name="email" placeholder="Email.." required="required" value="<?php echo set_value('email')?>" /></div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="name">
										Komentar</label>
									<textarea name="komentar" id="komentar" class="form-control" rows="9" cols="25" required="required" placeholder="Komentar.." value="<?php echo set_value('komentar')?>" ></textarea>
									<input type="hidden" name="id_post" id="id_post" value="<?php echo $id_post;?>" >
									<input type="hidden" name="url" id="url" value="<?php echo $detail['URL'];?>" >
								</div>
							</div>
							<div class="col-md-12">
								<button type="submit" class="btn btn-info pull-right" id="kirim_komentar" name="kirim_komentar" > Kirim Komentar</button>
								<br/>
								<br/>
								<br/>
							</div>
						</div>
					<?php echo form_close()?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style="border:1px solid #e6e2e2;">
				
			</div>
			<br/>
		</div>
<script>
$(document).ready(function(){
	
	pageLoad(1);

});

function pageLoad(page)
{
	
	var limit 	= 10;
	var key 	= '';
	var status 	= '';
	var id_post = "<?php echo $id_post;?>"
	
	$.ajax({
		url		: '<?php echo site_url()?>artikel/read/'+page,
		type	: 'POST',
		dataType: 'html',
		data	: {limit:limit,key:key,status:status,id_post:id_post},
		beforeSend : function(){
			
		},
		success : function(result)
		{
			$('#dtKomentar').empty().append(result);
		}
	});
}
</script>