<div class="row" >
	<?php 
		$no = ($paging['limit']*$paging['current'])-$paging['limit'];
		$no++;
		if($list->num_rows() > 0) { 
			foreach($list->result() as $row) { ?>
			<div class="col-md-12">
				<div class="panel panel-default">
					<a href="<?php echo site_url('artikel/detail/'.encode($row->ID)."/".$row->URL)?>" ><h2 class="panel-title" style="padding-top:20px;padding-left:15px;color:#772953;font-size:20px"><strong><?php echo $row->JUDUL;?></strong></h2></a>
					<div class="panel-body">
						<p style="font-size:12px;margin-top:0px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo  word_limiter(strip_tags($row->DESKRIPSI,20)) ?>&nbsp;&nbsp;&nbsp;</p><a href="<?php echo site_url('artikel/detail/'.encode($row->ID)."/".$row->URL)?>" class="label label-default"> Baca Selengkapnya <i class="fa fa-long-arrow-right"></i></a></p>
					</div>
					<?php $jmlah = $this->m_komentar->getCountKomentarByIdPost($row->ID_POST,1)?>
					<div class="panel-footer">
						<small><i class="fa fa-folder-open"></i> <label class="label label-info"><?php echo $row->NAMA_KTGR?></label> | <i class="fa fa-user"></i> Andi Rohandi | <i class="fa fa-calendar"></i>  <?php echo tgl_indo($row->TGL_INPUT);?> | <i class="fa fa-comments"></i> <?php echo $jmlah; ?> Komentar </small>
					</div>
				</div>
			</div>
		
		<?php $no++; } ?>
		<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
		<div class='row'>
			<div class='col-sm-4 col-xs-12' style='margin-top:30px;margin-bottom:10px'>
				Total <?php echo $paging['count_row'] ?> Post
			</div>
			<div class='col-sm-8 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:10px'>
				<?php echo $paging['list'] ?>
			</div>
		</div>
	<?php }else{
		echo "<div style='height:400px'><h3 style='margin-left:15px'>Artikel tidak ditemukan</h3></div>";
	} ?>
