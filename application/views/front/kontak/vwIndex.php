<style>
#title-page{
	background-image:url('<?php echo HTTP_F?>img/banner-title.png');
	background-repeat:no-repeat;
	min-height:50px;
	margin-left:-38px;
}
#title-page p{
	color:white;
	font-weight:bold;
	margin-left:20px;
	padding-top:3px;
}
.h1 small {
	font-size: 24px;
}
</style>

<div class="container">
	<br/>
	<!-- Tengah -->
	<div class="col-md-8">
		
			<div class="row" >
				<div class="col-md-12" >
				<div class="col-md-12 well" style="border-right:none;">
					<div id="title-page"><p>Kontak</p></div>
					<h3>Tinggalkan Pesan</h3>
					<div class="row">
						<div class="container">
							<div class="row">
								<div class="col-md-8">
									<div class="well well-sm">
										<?php if($this->session->flashdata('result')==2){ ?>
											<div class='alert alert-danger'>
												<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
												<?php echo validation_errors()?>
											</div>
										<?php }else if($this->session->flashdata('result')==3){ ?> 
											<div class='alert alert-danger'>
												<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
												<i class="fa fa-remove"></i> Pesan gagal dikirim. Silahkan untuk mencoba lagi
											</div>
										<?php }else if($this->session->flashdata('result')==1){ ?> 
											<div class='alert alert-success'>
												<button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button>
												<i class="fa fa-check"></i> Pesan berhasil dikirim
											</div>
										<?php } 
										echo form_open('kontak/create');?>
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label for="name">
															Nama</label>
														<input type="text" value="<?php echo set_value('nama')?>" class="form-control" id="nama" name="nama" placeholder="Nama.." required="required" />
													</div>
													<div class="form-group">
														<label for="email">
															Email</label>
														<div class="input-group">
															<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
															</span>
															<input type="email" class="form-control" id="email" name="email" placeholder="Email.." required="required" value="<?php echo set_value('email')?>" /></div>
													</div>
													<div class="form-group">
														<label for="subject">
															Subjek</label>
														<input type="text" class="form-control" id="subjek" name="subjek" placeholder="Subject.." required="required" value="<?php echo set_value('subjek')?>" />
													</div>
												</div>
												<div class="col-md-12">
													<div class="form-group">
														<label for="name">
															Pesan</label>
														<textarea name="pesan" id="pesan" class="form-control" rows="9" cols="25" required="required"
															placeholder="Pesan.." value="<?php echo set_value('pesan')?>" ></textarea>
													</div>
												</div>
												<div class="col-md-12">
													<button type="submit" class="btn btn-info pull-right" id="kirim_pesan" name="kirim_pesan" >
														Kirim Pesan</button>
												</div>
											</div>
										<?php echo form_close()?>
									</div>
								</div>
								<div class="col-md-4">
									<form>
									<legend><span class="glyphicon glyphicon-globe"></span> Kontak Info</legend>
									<address>
										Cibaligo Timur, RT 004 / RW 029<br>
										Kelurahan Cibeureum<br/>
										Cimahi Selatan - Cimahi<br/>
										40535<br/><br/>
										
										<label><i class="fa fa-facebook-official"></i></label> <a href="https://www.facebook.com/secangkirairdigurunpasir" target="_blank" >Andi Rohandi</a><br/>
										<label><i class="fa fa-twitter-square"></i></label> <a href="https://twitter.com/andicybers" target="_blank" >Andi Rohandi</a><br/>
										<label><i class="fa fa-whatsapp"></i></label> 0896-9042-8088<br/>
										<label><i class="fa fa-envelope"></i></label> andirohandi.android @ gmail.com<br/>
									</address>
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>