<style>
#title-page{
	background-image:url('<?php echo HTTP_F?>img/banner-title.png');
	background-repeat:no-repeat;
	min-height:50px;
	margin-left:-38px;
}
#title-page p{
	color:white;
	font-weight:bold;
	margin-left:20px;
	padding-top:3px;
}
.h1 small {
	font-size: 24px;
}
</style>

<div class="container">
	<br/>
	<!-- Tengah -->
	<div class="col-md-8">
		
			<div class="row" >
				<div class="col-md-12" >
				<div class="col-md-12 well" style="border-right:none;">
					<div id="title-page"><p>Portofolio</p></div>
					<div class="row">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h3 style="margin-top:0px;color:#772953;font-weight:bold;">Andi Rohandi</h3>
									<label>Cibaligo Timur RT 04 RW 29, Kel. Cibeureum, Kec. Cimahi Selatan, Cimahi 40535</label>
									<br/><br/>
									<table class="table table-responsive table-bordered table-hover">
										<tr>
											<th style="text-align:center;width:10%">No</th><th style="text-align:center;width:70%">Nama Aplikasi / Sistem Informasi</th><th style="text-align:center;width:70%">Tahun Pembuatan</th>
										</tr>
										<tr>
											<td style="text-align:center">1.</td><td>Pembuatan Company Profile PD. Ucok Tenda<br/>( <a href="http://ucoktenda.com/">http://ucoktenda.com/ )</a></td><td align="center">2015</td>
										</tr>
										<tr>
											<td style="text-align:center">2.</td><td>Pembuatan Sistem Informasi Dinas Pekerjaan Umum Kutai Timur<br/>( On Progress)</td><td align="center">2015 - 2016</td>
										</tr>
										<tr>
											<td style="text-align:center">3.</td><td>Pembuatan Website Portal Komunitas<br/>( On Progress )</td><td align="center">2015 - 2016</td>
										</tr>
										<tr>
											<td style="text-align:center">4.</td><td>Pembuatan Website Umroh Syahidun System<br/>( On Progress )</td><td align="center">2016</td>
										</tr>
										<tr>
											<td style="text-align:center">5.</td><td>Pembuatan Website pribadi Andirohandi.com<br/>( <a href="http://andirohandi.com/">http://andirohandi.com/</a> )</td><td align="center">2016</td>
										</tr>
									</table>
								</div>
								<br/><br/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>