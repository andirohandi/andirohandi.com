<style>

.form-group.success, .form-group.success input, .form-group.success select, .form-group.success textarea{color:#468847;border-color:#468847}
.form-group.error, .form-group.error input, .form-group.error select, .form-group.error textarea{color:#b94a48;border-color:#b94a48}
.drop-area{
  width:100px;
  height:45px;
  border: 1px solid #999;
  text-align: center;
  padding:10px;
  cursor:pointer;
}
#thumbnail img{
  width:155px;
  height:155px;
  margin:5px;
}
canvas{
  border:1px solid red;
}

</style>
<script src="<?php echo base_url()?>assets/tinymce/tinymce.min.js"></script>


<?php $id = (isset($post['ID_POST']));?>

<section class="content-header">
    <h1 class='pull-right"'>
        Post
        <small>Kontrol Post</small>
    </h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url('ar_1301/dashboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="<?php echo site_url('ar_1301/post')?>"> List Post</a></li>
		<li class="active"><?php echo $titlepage ?></li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
		<form action='<?php echo site_url('ar_1301/post/create')?>' method='POST' enctype='multipart/form-data'>
			<div class="row">
				<div id="msg" class='col-md-12'>
					<?php echo decode($msg);?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<label>Kategori Post</label>
								<select id="kategori" name="kategori" class='form-control' required>
									<option value="">PILIH KATEGORI</option>
									<?php $id_kat =  $id ? $post['KATEGORI'] : ''?>
									<?php echo get_kategori($id_kat)?>
								</select>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<label>Status Post</label>
						<select id="status" name="status" class='form-control' required>
							<option value='1' <?php echo $id  ? ($post['STATUS_POST']==1 ?'selected':'') : ''?> >AKTIF</option>
							<option value='0' <?php echo $id ? ($post['STATUS_POST']==0 ?'selected':'') : '' ?>>TIDAK AKTIF</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="form-group">
						<input type="hidden" name="id_post" id="id_post" class="form-control" value='<?php echo (isset($post['ID_POST']) ? encode($post['ID_POST']) : encode('')) ?>'>
						<label>Judul</label>
						<input type="text" class="form-control" id="judul" name="judul" value="<?php echo $id ? $post['JUDUL'] : ''?>" placeholder='Masukkan judul post . .' required>
					</div>
				</div>

				<div class="col-md-12 col-sm-12">
					<div class="form-group">
						<label>Deskripsi</label>
						<textarea name="deskripsi" id="deskripsi" class="form-control" style='height:250px'><?php echo (isset($post['ID_POST']) ? $post['DESKRIPSI'] : '') ?></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<div class="row">
							<label class="col-md-12 col-sm-12">Upload Image</label>
						</div>
						<input type="file" style="display:none" class="form-control" id="upload-image" name="upload-image" multiple="multiple"></input>
						
						<div id="upload" class="btn btn-default" >
							<?php $image = $post['THUMBNAIL'] != '' ? $post['THUMBNAIL'] : 'assets/img/no_poto.png';
								if(isset($post['IMAGE'])) {
									?>
										<i id='addImage' class='glyphicon glyphicon-plus hide'> Add</i>
										<div id="thumbnail"><img src="<?php echo site_url($image)?>" ></div>
									<?php
								} else {
									?>					
										<i id='addImage' class='glyphicon glyphicon-plus' style='width:90px'> Add</i>
										<div id="thumbnail"></div>
									<?php
								}
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<button type="submit" name='simpan' id='simpan' class='btn btn-success pull-right'><i class='fa fa-check'></i> Simpan Post</button>
					<a href='<?php echo site_url('AR_1301/post')?>' type="button" name='kembali' id='kembali' class='btn btn-default pull-right' style='margin-right:10px'><i class='fa fa-remove'></i> Kembali</a>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>

<script>
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});

$(document).ready(function(){
	var fileDiv = document.getElementById("upload");
		var fileInput = document.getElementById("upload-image");
		
		console.log(fileInput);
		
		fileInput.addEventListener("change",function(e){
		  var files = this.files
		  showThumbnail(files)
		},false)

		fileDiv.addEventListener("click",function(e){
		  $(fileInput).show().focus().click().hide();
		  e.preventDefault();
		},false)

		fileDiv.addEventListener("dragenter",function(e){
		  e.stopPropagation();
		  e.preventDefault();
		},false);

		fileDiv.addEventListener("dragover",function(e){
		  e.stopPropagation();
		  e.preventDefault();
		},false);

		fileDiv.addEventListener("drop",function(e){
		  e.stopPropagation();
		  e.preventDefault();

		  var dt = e.dataTransfer;
		  var files = dt.files;

		  showThumbnail(files)
		},false);
		
	});

function showThumbnail(files){
	  for(var i=0;i<files.length;i++){
		var file = files[i]
		var imageType = /image.*/

		if(!file.type.match(imageType)){
		  console.log("Not an Image");
		  continue;
		}

		var image = document.createElement("img");
		// image.classList.add("")
		var thumbnail = document.getElementById("thumbnail");
		image.file = file;
		
		while(thumbnail.hasChildNodes()) {
			thumbnail.removeChild(thumbnail.lastChild);
		}
		
		thumbnail.appendChild(image)
		
		$('#addImage').hide();
		
		var reader = new FileReader()
		reader.onload = (function(aImg){
		  return function(e){
			aImg.src = e.target.result;
		  };
		}(image))
		var ret = reader.readAsDataURL(file);
		var canvas = document.createElement("canvas");
		ctx = canvas.getContext("2d");
		image.onload= function(){
		  ctx.drawImage(image,100,100)
		}
	  }
	}
	$(function () {
		$('[data-toggle=\"tooltip\"]').tooltip()
	})

	
</script>
