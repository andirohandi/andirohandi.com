<br/>
<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box">
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover table-bordered">
					<thead>
						<th style="text-align:center;width:5%">No</th>
						<th style="text-align:center;width:40%">JUDUL</th>
						<th style="text-align:center;width:10%">TANGGAL</th>
						<th style="text-align:center;width:10%">KATEGORI</th>
						<th style="text-align:center;width:10">STATUS</th>
						<th style="text-align:center;width:25%">AKSI</th>
					</thead>
					<tbody>
						<?php $no = ($paging['limit']*$paging['current'])-$paging['limit'];
						$no++;
						if($list->num_rows() > 0) { 
							foreach($list->result() as $row) { ?>
							 <tr>
								<td style="text-align:center;"><?php echo $no ?></td>
								<td><?php echo $row->JUDUL ?></td>
								<td style='text-align:center'><?php echo tgl_indo($row->TGL_INPUT) ?></td>
								<td style="text-align:center;"><?php echo $row->NAMA_KTGR ?></td>
								<?php if($row->STATUS_POST==1){
									?>
									<td style='text-align:center'><a href='javascript:void(0)' class='btn btn-default' data-toggle='tooltip' data-placement='top' title='AKTIF'	><i class='fa fa-check'></i></a></td>
								<?php }else
								{ ?>
									<td style='text-align:center' ><a href='javascript:void(0)' class='btn btn-default' data-toggle='tooltip' data-placement='top' title='TIDAK AKTIF'	><i class='fa fa-remove'></i></a></td>
								<?php }  ?>
								<td style='text-align:center'>
									<a href='<?php echo site_url('home/detail/'.$row->URL.'/html')?>' class='btn btn-success'   data-toggle='tooltip'  data-placement='top' title='<?php echo $row->JUDUL ?>' ><i class='fa fa-eye' data-toggle='modal' data-target='#detailBarang'></i> Detail</a> 
									
									<a href='<?php echo site_url('AR_1301/post/input/'.encode($row->ID_POST))?>' class='btn btn-primary ' data-toggle='tooltip'  data-placement='top' title='<?php echo $row->JUDUL ?>' ><i class='fa fa-edit'></i> Edit</a> 
									
									<a href='javascript:void(0)' class='btn btn-danger' onclick='deletList("<?php echo encode($row->ID_POST);?>","<?php echo $row->IMAGE ?>")' data-toggle='tooltip' data-placement='top' title='<?php echo $row->JUDUL ?>'><i class='fa fa-trash'></i> Hapus</a></td>
									
							 </tr>
						<?php 	$no++;
							}
						} ?>
						<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
					</tody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class='row'>
    <div class='col-sm-4 col-xs-12' style='margin-top:5px;margin-bottom:10px'>
        Total <?php echo $paging['count_row'] ?> rows
    </div>
    <div class='col-sm-8 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:10px'>
        <?php echo $paging['list'] ?>
    </div>
</div>
	


<script>
	$(function () {
		$('[data-toggle=\"tooltip\"]').tooltip()
	})
	
	function deletList(i,x) {
		alertify.confirm("Apakah Anda Yakin Akan Menghapus Data ini ?", function (e) {
			if (e) {
				$.ajax({
					url		: "post/delete",
					type	: 'POST',
					dataType: 'json',
					data	: {i:i,x:x},
					
					beforeSend : function()
					{
					   
					},
					success : function(result){
						if(result.rs == '1') {
							pageLoad($('#current').val());
							alertify.success("<i class='glyphicon glyphicon-ok' ></i> Data berhasil dihapus");
						}
						else
						{
							$.sticky('Data gagal dihapus');
						}
					} 
				});
			}else{
			
			}
		});
	}
</script>