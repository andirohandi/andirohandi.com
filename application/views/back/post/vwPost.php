<style>
.error{
	color: #FB3A3A;
    padding: 0;
    text-align: left;
}
#kanan-menu{
	float:right;

}

</style>
<section class="content-header">
    <h1 class='pull-right"'>
        Post
        <small>Kontrol Post</small>
    </h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url('ar_1301/dashboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">List Post</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-1 col-xs-12" style="margin-top:5px;">
			<a href="<?php echo site_url('ar_1301/post/input')?>" class="btn btn-success" ><i class="fa fa-plus"></i> New Post</a>
		</div>
		
		<div class="col-md-3 col-xs-12 pull-right" style="margin-top:5px;">
			<div class="input-group custom-search-form">
				<input class="form-control" type="text" placeholder="Search..." name='key' id='key' onchange='pageLoad(1)'>
				<span class="input-group-btn">
					<button class="btn btn-default" type="button">
					<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</div>
		<div class="col-md-2 col-xs-12 pull-right" style="margin-top:5px;">
			<select class="form-control col-md-6 col-xs-12" name='kategori' id='kategori' onchange='pageLoad(1)'>
				<option value="">PILIH KATEGORI</option>
				<?php echo get_kategori()?>
			</select>
		</div>
		<div class="col-md-1 col-xs-12 pull-right" style="margin-top:5px;">
			<select class="form-control col-md-6 col-xs-12" name='limit' id='limit' onchange='pageLoad(1)'>
				<option value="5">5 rows</option>
				<option value="10">10 rows</option>
				<option value="25">25 rows</option>
				<option value="50">50 rows</option>
				<option value="100">100 rows</option>
			</select>
		</div>
		
	</div>

	<div id="dtPost">
		<div class='row' id='loading' style='display:none'>
			<div class='col-md-12'>
				<div class="box">
					<div class="box-header">
						
					</div>
					<div class="box-body">
					 
					</div>
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	pageLoad(1);
})

function test(l) {
	var des = document.getElementById('des');
	alert(des);
}
function pageLoad(page)
{
	
	var limit 	= $('#limit').val();
	var key 	= $('#key').val();
	var kategori= $('#kategori').val();
	
	$.ajax({
		url		: '<?php echo site_url()?>/ar_1301/post/read/'+page,
		type	: 'POST',
		dataType: 'html',
		data	: {limit:limit,key:key,kategori:kategori},
		beforeSend : function(){
			$('#loading').fadeIn('slow');
		},
		success : function(result)
		{
			$('#dtPost').empty().append(result);
		}
	});
}

</script>