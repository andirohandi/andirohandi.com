<style>
.error{
	color: #FB3A3A;
    padding: 0;
    text-align: left;
}
#kanan-menu{
	float:right;

}

</style>


<section class="content-header">
    <h1 class='pull-right"'>
        Komentar
        <small>Kontrol Komentar</small>
    </h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url('AR_1301/dashboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">List Komentar</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-2 col-xs-12" style="margin-top:5px;">
			<select class="form-control col-md-6 col-xs-12" name='limit' id='limit' onchange='pageLoad(1)'>
				<option value="5">5 rows</option>
				<option value="10">10 rows</option>
				<option value="25">25 rows</option>
				<option value="50">50 rows</option>
				<option value="100">100 rows</option>
			</select>
		</div>
		<div class="col-md-3 col-xs-12 pull-right" style="margin-top:5px;">
			<div class="input-group custom-search-form">
				<input class="form-control" type="text" placeholder="Search..." name='key' id='key' onchange='pageLoad(1)'>
				<span class="input-group-btn">
					<button class="btn btn-default" type="button">
					<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</div>
		<div class="col-md-2 col-xs-12 pull-right" style="margin-top:5px;">
			<select class="form-control col-md-6 col-xs-12" name='status' id='status' onchange='pageLoad(1)'>
				<option value="">-- Pilih Status --</option>
				<option value="0">PENDING</option>
				<option value="1">AKTIF</option>
				<option value="2">TIDAK AKTIF</option>
			</select>
		</div>
	</div>

	<div id="dtKomentar">
		<div class='row' id='loading' style='display:none'>
			<div class='col-md-12'>
				<div class="box">
					<div class="box-header">
						
					</div>
					<div class="box-body">
					 
					</div>
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
$(document).ready(function()
{
	
	pageLoad(1);
	
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})
	
	
	alertify.set({ buttonReverse: true });
	
	alertify.set({ labels: {
		ok     : "Ya",
		cancel : "Tidak"
		 
		} 
	});
	
});

function pageLoad(page)
{
	
	var limit 	= $('#limit').val();
	var key 	= $('#key').val();
	var status 	= $('#status').val();
	
	$.ajax({
		url		: '<?php echo site_url()?>AR_1301/komentar/read/'+page,
		type	: 'POST',
		dataType: 'html',
		data	: {limit:limit,key:key,status:status},
		beforeSend : function(){
			$('#loading').fadeIn('slow');
		},
		success : function(result)
		{
			$('#dtKomentar').empty().append(result);
		}
	});
}

function hapusKomentar(i)
{
	alertify.confirm("Apakah Anda Yakin Akan Menghapus Data ini ?", function (e) {
		if (e) {
			$.ajax({
				url		: "<?php echo site_url()?>ar_1301/komentar/delete",
				type	: 'POST',
				dataType: 'json',
				data	: {i:i},
				
				beforeSend : function()
				{
				   
				},
				success : function(result){
					if(result.rs == '1') {
						pageLoad($('#current').val());
						alertify.success("<i class='glyphicon glyphicon-ok' ></i> Data berhasil dihapus");
					}
					else
					{
						$.sticky('Data gagal dihapus');
					}
				} 
			});
		}else{
		
		}
	});
}

function cekNamaKategori()
{
	var nama = $('#svNama').val();
	
	$.ajax({
		url		: '<?php echo site_url()?>ar_1301/kategori/readCheckName/'+nama,
		type	: 'POST',
		dataType: 'JSON',
		success : function(data)
		{
			if(data.result==1)
			{
				alertify.alert("Maaf! Nama kategori sudah ada");
				$('#svNama').val('');
			}
		}
	});
}

</script>

