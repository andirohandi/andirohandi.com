

<br/>
<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box">
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;width:5%">No</th>
							<th style="text-align:center;width:25%">JUDUL POSTINGAN</th>
							<th style="text-align:center;width:25%">KOMENTAR</th>
							<th style="text-align:center;width:15%">EMAIL</th>
							<th style="text-align:center;width:10%">STATUS</th>
							<th style="text-align:center;width:20%">AKSI</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = ($paging['limit']*$paging['current'])-$paging['limit'];
						$no++;
						if($list->num_rows() > 0) { 
							foreach($list->result() as $row) { ?>
							 <tr>
								<td align="center"><?php echo $no ?></td>
								<td><?php echo $row->JUDUL_POST ?></td>
								<td><?php echo $row->KOMENTAR ?></td>
								<td><?php echo $row->EMAIL ?></td>
								<?php 
									if($row->STATUS==0){
										$status = "<label class='label label-warning'>Pending</label>";
									}else if($row->STATUS==1){
										$status = "<label class='label label-success'>Aktif</label>";
									}else if($row->STATUS==2){
										$status = "<label class='label label-danger'>Tidak Aktif</label>";
									}  
								?>
								<td style='text-align:center'><?php echo $status; ?></td>
								
								<td style='text-align:center'><a href='javascript:void(0)' class='btn btn-primary '><i class='fa fa-edit' data-toggle='modal' data-target='#editKategori'></i> Detail</a> 
								<a href='javascript:void(0)' class='btn btn-danger' onclick='hapusKomentar("<?php echo $row->ID_K;?>")'><i class='fa fa-trash'></i> Hapus</a></td>
							 </tr>
						<?php 	$no++;
							}
						} ?>
						<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
					</tody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class='row'>
    <div class='col-sm-4 col-xs-12' style='margin-top:5px;margin-bottom:10px'>
        Total <?php echo $paging['count_row'] ?> rows
    </div>
    <div class='col-sm-8 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:10px'>
        <?php echo $paging['list'] ?>
    </div>
</div>

<script>
	$(function () {
		$('[data-toggle=\"tooltip\"]').tooltip()
	})
</script>