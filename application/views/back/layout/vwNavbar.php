<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title; ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo HTTP_B ?>bootstrap/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        
        <link rel="stylesheet" href="<?php echo HTTP_B ?>plugins/iCheck/all.css">
        <link rel="stylesheet" href="<?php echo HTTP_B ?>plugins/select2/select2.min.css">
        <link rel="stylesheet" href="<?php echo HTTP_B ?>plugins/datepicker/datepicker3.css">
        <link rel="stylesheet" href="<?php echo HTTP_B ?>plugins/daterangepicker/daterangepicker-bs3.css">
        <link rel="stylesheet" href="<?php echo HTTP_B ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_B ?>dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo HTTP_B ?>dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" href="<?php echo HTTP_B ?>dist/css/skins/skin-green.min.css">
		<link rel="shortcut icon" href="<?php echo HTTP_F ?>img/logo.ico">
        <script src="<?php echo HTTP_B ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo HTTP_B ?>plugins/jQuery/jquey_validate.js"></script>
        <script src="<?php echo HTTP_B ?>plugins/jQueryUI/jquery-ui.min2.js"></script>
		
		<link href="<?php echo base_url();?>assets/alertify/css/alertify.core.css" rel="stylesheet">
		<link href="<?php echo base_url();?>assets/alertify/css/alertify.default.css" rel="stylesheet">

    </head>
    <body class="hold-transition skin-green sidebar-mini">
        <div class="wrapper">

            <!-- Navbar header --!-->
            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo site_url('')?>" class="logo" style='background-color:#097054'>
                    <span class="logo-mini"><b>AR</b></span>
                    <span class="logo-lg"><b> andirohandi.com</b></span>
                </a>
                <nav class="navbar navbar-static-top" role="navigation" style='background-color:#097054'>
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo HTTP_B ?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs">Andi Rohandi</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img src="<?php echo HTTP_F ?>img/logo2.png" class="img-circle" alt="User Image">
                                        <p>
                                            Andi Rohandi
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    </li>
                                    <li class="user-body">
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Followers</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Sales</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Friends</a>
                                        </div>
                                    </li>
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo site_url('ar_1301/auth/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>