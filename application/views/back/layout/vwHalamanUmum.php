<section class="content-header">
    <h1>
        Halaman Umum Content
        <small>Content Data</small>
    </h1>
</section>
<br/>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Halaman Utama</li>
</ol>

<section class="content">
    <!-- Main row -->

    <div class='row'>

        <div class='col-sm-4 col-xs-12' style='margin-top:10px'>
            <a class='btn btn-success col-xs-12 col-md-4 btn-sm' href=''><i class='fa fa-plus'></i> Tambah Data</a>
            <br/>
        </div>
        <div class='col-sm-3 col-xs-12 pull-right' style='margin-top:10px'>
            <div class="input-group pull-right">
                <input type="text" name="table_search" class="form-control input-sm col-sm-4 col-xs-12" placeholder="Search...">
                <div class="input-group-btn">
                    <button class="btn btn-default btn-sm"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class='row'>
        <br/>
        <div class='col-sm-12'>
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th>ID</th>
                            <th>User</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Reason</th>
                            <th>Action</th>
                        </tr>
                        <tr>
                            <td>183</td>
                            <td>John Doe</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-success">Approved</span></td>
                            <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                            <td>
                                <button class='btn btn-info  btn-sm'><i class='fa fa-edit'></i> Edit</button>
                                <button class='btn btn-success  btn-sm'><i class='fa fa-eye'></i> Detail</button>
                                <button class='btn btn-danger  btn-sm'><i class='fa fa-trash'></i> Hapus</button>
                            </td>
                        </tr>
                        <tr>
                            <td>219</td>
                            <td>Alexander Pierce</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-warning">Pending</span></td>
                            <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                            <td>
                                <button class='btn btn-info  btn-sm'><i class='fa fa-edit'></i> Edit</button>
                                <button class='btn btn-success  btn-sm'><i class='fa fa-eye'></i> Detail</button>
                                <button class='btn btn-danger  btn-sm'><i class='fa fa-trash'></i> Hapus</button>
                            </td>
                        </tr>
                        <tr>
                            <td>657</td>
                            <td>Bob Doe</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-primary">Approved</span></td>
                            <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                            <td>
                                <button class='btn btn-info  btn-sm'><i class='fa fa-edit'></i> Edit</button>
                                <button class='btn btn-success  btn-sm'><i class='fa fa-eye'></i> Detail</button>
                                <button class='btn btn-danger  btn-sm'><i class='fa fa-trash'></i> Hapus</button>
                            </td>
                        </tr>
                        <tr>
                            <td>175</td>
                            <td>Mike Doe</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-danger">Denied</span></td>
                            <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                            <td>
                                <button class='btn btn-info  btn-sm'><i class='fa fa-edit'></i> Edit</button>
                                <button class='btn btn-success  btn-sm'><i class='fa fa-eye'></i> Detail</button>
                                <button class='btn btn-danger  btn-sm'><i class='fa fa-trash'></i> Hapus</button>
                            </td>
                        </tr>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-4'>
            Total 5 rows dari 100 rows
        </div>
        <div class='col-sm-8 pull-right'>
            <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
        </div>
    </div>
</section>