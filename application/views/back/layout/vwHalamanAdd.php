

<section class="content-header">
    <h1 class='pull-right"'>
        Halaman Content Add
        <small>Input Data</small>
    </h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Halaman Utama</li>
	</ol>
</section>

<section class="content">
    <!-- Main row -->
    <div class='row'>
        <br/>
        <div class='col-md-12 col-sm-12'>
            <div class="box box-primary">
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label>Minimal</label>
                            <select class="form-control">
                                <option selected="selected">Alabama</option>
                                <option>Alaska</option>
                                <option>California</option>
                                <option>Delaware</option>
                                <option>Tennessee</option>
                                <option>Texas</option>
                                <option>Washington</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" id="exampleInputFile">
                            <p class="help-block">Example block-level help text here.</p>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Check me out
                            </label>
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right" ><i class='fa fa-save'></i> Simpan</button>
                        <button type="submit" class="btn btn-default pull-right" style='margin-right:10px'><i class='fa fa-undo'></i> Kembali</button>
                    </div>
                </form>
            </div><!-- /.box -->
        </div>
    </div>
</section>