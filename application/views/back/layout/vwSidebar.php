
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo HTTP_F ?>img/logo2.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Andi Rohandi</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Admin</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header">MENU UTAMA</li>
            <li class="<?php echo $pg == 'dashboard' ? 'active' : '' ?>" >
                <a href="<?php echo site_url('dashboard') ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
			<li class="treeview <?php echo $pg == 'kategori'  ? 'active' : '' ?>">
				<a href="javascript:void(0)">
					<i class="fa fa-bookmark"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li class='<?php echo $pg == 'kategori' ? 'active' : '' ?>' ><a href="<?php echo site_url('ar_1301/kategori')?>"><i class="fa fa-angle-double-right"></i> Kategori</a></li>
				</ul>
			</li>
			<li class="<?php echo $pg == 'post' ? 'active' : '' ?>" >
                <a href="<?php echo site_url('ar_1301/post') ?>">
                    <i class="fa fa-bookmark"></i> <span>Post</span>
                </a>
            </li>
			<li class="<?php echo $pg == 'komentar' ? 'active' : '' ?>" >
                <a href="<?php echo site_url('ar_1301/komentar') ?>">
                    <i class="fa fa-bookmark"></i> <span>Komentar</span>
                </a>
            </li>
			<li class="<?php echo $pg == 'testimoni' ? 'active' : '' ?>" >
                <a href="<?php echo site_url('ar_1301/testimoni') ?>">
                    <i class="fa fa-bookmark"></i> <span>Testimoni</span>
                </a>
            </li>
			<li class="<?php echo $pg == 'saran' ? 'active' : '' ?>" >
                <a href="<?php echo site_url('ar_1301/saran') ?>">
                    <i class="fa fa-bookmark"></i> <span>Saran</span>
                </a>
            </li>
        </ul>
    </section>
</aside>

<div class="content-wrapper">