

<br/>
<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box">
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover table-bordered">
					<thead>
						<tr>
							<th style="text-align:center;width:5%">No</th>
							<th style="text-align:center;width:15%">NAMA KATEGORI</th>
							<th style="text-align:center;width:40%">KETERANGAN</th>
							<th style="text-align:center;width:10%">STATUS</th>
							<th style="text-align:center;width:15%">AKSI</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = ($paging['limit']*$paging['current'])-$paging['limit'];
						$no++;
						if($list->num_rows() > 0) { 
							foreach($list->result() as $row) { ?>
							 <tr>
								<td align="center"><?php echo $no ?></td>
								<td><?php echo $row->NAMA_KTGR ?></td>
								<td><?php echo $row->KETERANGAN ?></td>
								<?php if($row->STATUS==1){
									?>
									<td style='text-align:center'><a href='javascript:void(0)' class='btn btn-default ' data-toggle='tooltip' data-placement='top' title='AKTIF'	><i class='fa fa-check'></i></a></td>
								<?php }else
								{ ?>
									<td style='text-align:center' ><a href='javascript:void(0)' class='btn btn-default ' data-toggle='tooltip' data-placement='top' title='TIDAK AKTIF'	><i class='fa fa-remove'></i></a></td>
								<?php }  ?>
								<td style='text-align:center'><a href='javascript:void(0)' class='btn btn-primary ' onclick='getEditKategori("<?php echo $row->ID;?>","<?php echo $row->NAMA_KTGR;?>","<?php echo $row->STATUS;?>","<?php echo $row->KETERANGAN;?>")'  data-toggle='tooltip'  data-placement='top' title='<?php echo $row->NAMA_KTGR ?>' ><i class='fa fa-edit' data-toggle='modal' data-target='#editKategori'></i> Edit</a> 
								<a href='javascript:void(0)' class='btn btn-danger' onclick='hapusKategori("<?php echo $row->ID;?>")' data-toggle='tooltip' data-placement='top' title='<?php echo $row->NAMA_KTGR ?>'><i class='fa fa-trash'></i> Hapus</a></td>
							 </tr>
						<?php 	$no++;
							}
						} ?>
						<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
					</tody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class='row'>
    <div class='col-sm-4 col-xs-12' style='margin-top:5px;margin-bottom:10px'>
        Total <?php echo $paging['count_row'] ?> rows
    </div>
    <div class='col-sm-8 col-xs-12 pull-right' style='margin-top:5px;margin-bottom:10px'>
        <?php echo $paging['list'] ?>
    </div>
</div>

<script>
	$(function () {
		$('[data-toggle=\"tooltip\"]').tooltip()
	})
</script>