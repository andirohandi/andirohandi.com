<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kontak extends CI_Model {

	private $table = "tbl_pesan";
	private $id = "ID";

	public function getInsert($dt)
	{
		$this->db->set($dt);
		$this->db->insert($this->table);
	}
	
	public function getInsertSaran($dt)
	{
		$this->db->set($dt);
		$this->db->insert('tbl_saran');
	}
	
}