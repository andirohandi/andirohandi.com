<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_komentar extends CI_Model {

	private $table = "tbl_komentar";
	private $id = "ID";
	
	
	// Create
	public function getInsert($dt)
	{
		$this->db->set($dt);
		$this->db->insert($this->table);
	}
	
	// Read
	function getSelect($status='', $key='', $limit='', $offset='', $where='') {
		$this->db->select('a.*');
		$this->db->select(' a.STATUS as STATUS_K, a.ID AS ID_K, b.ID AS ID_POST, b.JUDUL AS JUDUL_POST',FALSE);
		$this->db->join('tbl_post b', 'a.POST_ID = b.ID', 'left');
		$this->db->order_by('a.ID', 'DESC');
		
		if($key!='')
		{
			$this->db->like("a.KOMENTAR",$key);
			$this->db->or_like("a.EMAIL",$key);
			$this->db->or_like("b.JUDUL",$key);
		}else{}
		
		if($status!='')
		{
			$this->db->where('a.STATUS',$status);
		}else{}
		
		if($where){
			$this->db->where($where);
		}
		
		if(!$limit && !$offset) $query = $this->db->get('tbl_komentar a');
		else $query = $this->db->get('tbl_komentar a', $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	
	function getSelectKomentarByIdPost($id,$status,$limit,$offset,$group=""){
		$this->db->where('POST_ID',$id)->where('STATUS',$status);
		$this->db->order_by('ID','DESC');
		
		if($group){
			$this->db->group_by("EMAIL");
		}
		
		if(!$limit && !$offset) $query = $this->db->get('tbl_komentar');
		else $query = $this->db->get('tbl_komentar', $limit, $offset);
		
		
		
		return $query;
		$query->free_result();
	}
	
	function getCountKomentarByIdPost($id,$status){
		$query = $this->db->where('POST_ID',$id)->where('STATUS',$status)->get('tbl_komentar');
		return $query->num_rows();
		$query->free_result();
	}
	
	// Update
	public function getUpate($dt,$id)
	{
		$this->db->set($dt)
			->where('ID',$id)
			->update($this->table);
	}
	
	// Delete
	public function getDelete($id)
	{
		$this->db->where('ID',$id)
			->delete($this->table);
	}

	// Fungsi - Fungsi Tambahan -------------------------
	
	// Menghitung Jumlah Kategori
	function getSelectCount($status='',$key='') {
		$this->db->select('a.*');
		$this->db->select(' a.STATUS as STATUS_K, a.ID AS ID_K, b.ID AS ID_POST, b.JUDUL AS JUDUL_POST',FALSE);
		$this->db->join('tbl_post b', 'a.POST_ID = b.ID', 'left');
		
		if($key!='')
		{
			$this->db->like("a.KOMENTAR",$key);
			$this->db->or_like("b.JUDUL",$key);
		}else{}
		
		if($status!='')
		{
			$this->db->where('a.STATUS',$status);
		}else{}
		
		$query = $this->db->get('tbl_komentar a');
		
		return $query->num_rows();
		$query->free_result();
	}
	
	// Cek nama kategori yang masih availabe
	function getDbName($url)
	{
		return $this->db->where("URL",$url)
			->get($this->table);
	}
	
	//membuat select option
	function getSelectOption($where=''){
		if($where) $this->db->where($where);
		
		$query = $this->db->get($this->table);
		$result = $query->result();
		return $result;
	}
}