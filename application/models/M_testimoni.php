<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_testimoni extends CI_Model {

	private $table = "tbl_testimoni";
	private $id = "ID";

	public function getInsert($dt)
	{
		$this->db->set($dt);
		$this->db->insert($this->table);
	}
	
	function getSelect($where='', $limit='', $offset='', $kategori='') {

		$this->db->order_by('ID','DESC');
		if($where)
			$this->db->where($where);
		
		if(!$limit && !$offset)
			$query = $this->db->get('tbl_testimoni');
		else                                     
			$query = $this->db->get('tbl_testimoni', $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	function count($where='',$kategori='') {
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get('tbl_testimoni');
		
		return $query->num_rows();
		$query->free_result();
	}
	
}