<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_post extends CI_Model {

	private $table = "tbl_post";
	private $id = "ID";

	// Create
	public function getInsert($dt)
	{
		$this->db->set($dt);
		$this->db->insert($this->table);
	}
	
	function getSelect($where='', $limit='', $offset='', $kategori='') {
		$this->db->select('a.*');
		
		$this->db->select(' a.STATUS as STATUS_POST, a.ID AS ID_POST, b.NAMA_KTGR AS NAMA_KTGR',FALSE);
		$this->db->join('tbl_kategori b', 'a.KATEGORI = b.ID', 'left');
		$this->db->order_by('a.ID', 'DESC');

		if($kategori) {
			$this->db->where('a.KATEGORI',$kategori);
		}
	
		if($where)
			$this->db->where($where);
		
		if(!$limit && !$offset)
			$query = $this->db->get('tbl_post a');
		else                                     
			$query = $this->db->get('tbl_post a', $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	function getSelectById($where=array()){
		$this->db->select('a.*');
		
		$this->db->select(' a.STATUS as STATUS_POST, a.ID AS ID_POST, b.NAMA_KTGR AS NAMA_KTGR',FALSE);
		$this->db->join('tbl_kategori b', 'a.KATEGORI = b.ID', 'left');
		
		return $this->db->where($where)->get('tbl_post a')->row_array();
		
	}
	
	function getSelectHome($limit='', $offset='', $where='') {
		$this->db->select('a.*');
		
		$this->db->select(' a.STATUS as STATUS_POST, a.ID AS ID_POST',FALSE);
		$this->db->join('tbl_kategori b', 'a.KATEGORI = b.ID', 'left');
		$this->db->order_by('a.ID', 'DESC');
		$this->db->where('a.STATUS',1);
		
		if($where) {
			$this->db->where($where);
		}
		
		$query = $this->db->get('tbl_post a', $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	// Update
	public function getUpdate($dt,$id)
	{
		$this->db->set($dt);
		$this->db->where('ID',$id);
		$this->db->update($this->table);
	}
	
	function getDetail($where=''){
		$data = array();
		
		$this->db->select('a.*');
		
		$this->db->select(' a.STATUS as STATUS_POST, a.ID AS ID_POST',FALSE);
		$this->db->join('tbl_kategori b', 'a.KATEGORI = b.ID', 'left');
		
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get('tbl_post a');
		
		$data = $query->row_array();
		$query->free_result();
		
		return $data;
	}
	
	function count($where='',$kategori='') {
		
		$this->db->select('a.*');
		
		$this->db->select(' a.STATUS as STATUS_POST, a.ID AS ID_POST',FALSE);
		$this->db->join('tbl_kategori b', 'a.KATEGORI = b.ID', 'left');
		
		if($where)
			$this->db->where($where);
		if($kategori)
			$this->db->where('KATEGORI',$kategori);
		
		$query = $this->db->get('tbl_post a');
		
		return $query->num_rows();
		$query->free_result();
	}

	function get($where='') {
		$data = array();
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get('tbl_post tbl');
		
		$data = $query->row_array();
		$query->free_result();
		
		return $data;
	}
}