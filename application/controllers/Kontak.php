<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model(array('m_kategori','m_post','m_komentar','m_kontak'));
		$this->load->library('form_validation');

	}

	public function index() {
		
		$data['pg']		= "kontak";
		$data['title']	= "Kontak | ar.c";
		
		$this->load->view('front/layout/vwHeader',$data);
		$this->load->view('front/kontak/vwIndex');
		$this->load->view('front/layout/vwBottomBar');
		$this->load->view('front/layout/vwSideBar');
		$this->load->view('front/layout/vwFooter');
	}
	
	function create(){
		if(!isset($_POST['kirim_pesan'])){
			redirect('e/forbidden','refresh');
		}else{
			
			$this->form_validation->set_rules('nama','Nama','trim|required');
			$this->form_validation->set_rules('email','Email','trim|required');
			$this->form_validation->set_rules('subjek','Subjek','trim|required');
			$this->form_validation->set_rules('pesan','Pesan','trim|required');
			
			if ($this->form_validation->run() == FALSE) {
				$dt = array('result'=>2);
				$this->session->set_flashdata($dt);
				$this->index();
			}else{
				
				$nama	= $this->input->post('nama',true);
				$email	= $this->input->post('email',true);
				$subjek	= $this->input->post('subjek',true);
				$pesan	= $this->input->post('pesan',true);
				
				$this->db->trans_begin();
				
				$data = array(
					'ID'		=> '',
					'NAMA'		=> $nama,
					'SUBJEK'	=> $subjek,
					'PESAN'		=> $pesan,
					'EMAIL'		=> $email
				);
				
				$this->m_kontak->getInsert($data);
				
				if($this->db->trans_status() === false) {
					$this->db->trans_rollback();
					$dt = array('result'=>3);
					$this->session->set_flashdata($dt);
					redirect('kontak','refresh');
				}else{
					$this->db->trans_commit();
					$dt = array('result'=>1);
					$this->session->set_flashdata($dt);
					redirect('kontak','refresh');
				}
			}
		}
	}
	
	function create_saran(){
		if(!isset($_POST['kirim_saran'])){
			redirect('e/forbidden','refresh');
		}else{
			
			$this->form_validation->set_rules('email_komentar','Email','trim|required');
			$this->form_validation->set_rules('isi_komentar','Saran','trim|required');
			$this->form_validation->set_rules('url','Url','trim|required');
			
			if ($this->form_validation->run() == FALSE) {
				$dt = array('result'=>5);
				$this->session->set_flashdata($dt);
				$this->index();
			}else{
				
				$email	= $this->input->post('email_komentar',true);
				$pesan	= $this->input->post('isi_komentar',true);
				$url	= $this->input->post('url',true);
				
				$this->db->trans_begin();
				
				$data = array(
					'ID'		=> '',
					'SARAN'		=> $pesan,
					'EMAIL'		=> $email
				);
				
				$this->m_kontak->getInsertSaran($data);
				
				if($this->db->trans_status() === false) {
					$this->db->trans_rollback();
					$dt = array('result'=>6);
					$this->session->set_flashdata($dt);
					redirect($url,'refresh');
				}else{
					$this->send_email($email);
					$this->db->trans_commit();
					$dt = array('result'=>4);
					$this->session->set_flashdata($dt);
					redirect($url,'refresh');
				}
			}
		}
	}
	
	function send_email($email){
		
		$msg	= "<b>Salam,</b><br/>";
		$msg	.= "Terimakasih telah meninggalkan saran di  <a href='".site_url('')."'>andirohandi.com</a>. Semoga kami bisa menjalankan apa yang anda sarankan";
		$msg	.= "<br/><br/>Salam,";
		$msg	.= "<br/>Admin andirohandi.com,";
		$this->load->library('email');
	    $this->email->from('andirohandi.android@gmail.com', 'andirohandi.com');
		$this->email->to($email]);
	    $this->email->subject("Terimakasih Atas Sarannya");
	    $this->email->message($msg);
	   
	    if ($this->email->send())
	        return true;
	    else
	        return false;
	}
}
