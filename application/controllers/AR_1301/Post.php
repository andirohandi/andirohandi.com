<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')){
			redirect('beranda');
		}
		
		$this->load->model(array('m_kategori','m_post'));

	}
	
    public function index() {
		
        $data['title'] = 'Post';
        $data['pg'] = 'post';

        $this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/post/vwPost');
        $this->load->view('back/layout/vwFooter');
    }
	
	public function input($id='',$msg='')
	{
		
		if(decode($id)!=0) {
			$id = decode($id);
			$title = "Ubah Post";
		}	
		else {
			$id = '';
			$title = "Input Post";
		}
		
		$data['post'] = $this->m_post->getDetail(array('a.ID' => $id));
		
		$data['title'] 		= $title;
		$data['titlepage'] 	= $title;
		$data['pg'] 		= 'post';
		$data['msg'] 		= $msg;
		
		$this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/post/vwInput');
        $this->load->view('back/layout/vwFooter');
	}
	
	public function create(){
		$this->load->helper('file');
		$ID 		= decode($_POST['id_post']);
		$JUDUL 		= trim($this->input->post('judul',true));
		$KATEGORI 	= trim($this->input->post('kategori',true));
		
		$DESKRIPSI 	= trim($this->input->post('deskripsi',true));
		$TGL_INPUT 	= date('Y-m-d');
		$STATUS		= trim($this->input->post('status',true));
		$psn 		= '';
		$url_judul  = trim(preg_replace('^[\\\\/:\*\?!.,;\"<>\|]^',' ',$JUDUL));
		$URL		= strtolower(str_replace(' ','-',$url_judul));
		
		
		$config['upload_path'] = './uploads/images/';
		$config['allowed_types'] =  'gif|jpg|png';
		$config['max_size'] = '5120';
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		$THUMBNAIL = '';
		$IMAGE = '';
		
		if($this->upload->do_upload('upload-image'))
		{
			$file = $this->upload->data();
			
			$IMAGE = 'uploads/images/'.$file['file_name'];
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = $file['full_path'];
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 128;
			$config['height'] = 128;
			$config['new_image'] = './uploads/thumbnails/';

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			$THUMBNAIL = 'uploads/thumbnails/'.$file['raw_name'].'_thumb'.$file['file_ext'];
			
			$this->image_lib->clear();
		} else {
			$type = get_mime_by_extension($_FILES['upload-image']['name']);
			
			if(($type != 'image/jpeg' || $type != 'image/png' || $type != 'image/gif') && $_FILES['upload-image']['size'] > $config['max_size']) {
				$error = $this->upload->display_errors();
				$error = encode('<div class="alert alert-danger" role="alert"><h4><i class="glyphicon glyphicon-remove"></i>' . $error . '</h4></div>');
				redirect(base_url().'ar_1301/post/input/'.encode(0).'/'.$error);
			}
		}
		
		$data = array(
			'ID' 		=> '',
			'JUDUL' 	=> $JUDUL,
			'KATEGORI'	=> $KATEGORI,
			'DESKRIPSI'	=> $DESKRIPSI,
			'TGL_INPUT' => $TGL_INPUT,
			'URL' 		=> $URL,
			'STATUS' 	=> $STATUS,
			'IMAGE' 	=> $IMAGE,
			'THUMBNAIL' => $THUMBNAIL
		);
		
		$data2 = array(
			'JUDUL' 	=> $JUDUL,
			'KATEGORI'	=> $KATEGORI,
			'DESKRIPSI'	=> $DESKRIPSI,
			'URL' 		=> $URL,
			'STATUS' 	=> $STATUS,
			'IMAGE' 	=> $IMAGE,
			'THUMBNAIL' => $THUMBNAIL
		);
		
		$this->db->trans_begin();
		
		if($_POST['id_post'] != ''){

			$this->m_post->getUpdate($data2, $ID);
			
			$url = $_POST['id_post'];
			$psn = "Diubah";
		}else{
			$this->m_post->getInsert($data);		
			$url = encode(0);
			$psn = "Disimpan";
		}
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$msg = encode('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button><i class="glyphicon glyphicon-remove"></i> Data Gagal Disimpan'.$psn.'</div>');
			redirect(base_url().'ar_1301/post/input/'.$url.'/'.$msg);
		} else {
			$this->db->trans_commit();
			$msg = encode('<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert" area-hidden="true">x</button><i class="fa fa-check"></i> Data Berhasil '.$psn.'</div>');
			redirect(base_url().'ar_1301/post/input/'.$url.'/'.$msg);
		}
		
	}
	
	function read($pg=1) {		
		$kategori	= $_POST['kategori'];
		$limit 		= $_POST['limit'];
		$key_text 	= $_POST['key'];
		$offset 	= ($limit*$pg)-$limit;

		$like = "a.JUDUL like '%$key_text%'";
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_post->count($like,$kategori);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_post->getSelect($like, $limit, $offset, $kategori);
		
		$this->load->view('back/post/vwList', $data);
	}
	
	function delete() {
		$id = decode($_POST['i']);
		$image = $_POST['x'];
		$this->db->trans_begin();
		
		$pos = $this->m_post->get(array('ID' => $id));
		
		if($pos['THUMBNAIL']) {
			
			//hapus file foto
			unlink(FCPATH.$pos['IMAGE']);
			unlink(FCPATH.$pos['THUMBNAIL']);
		}else{
			
		}
		
		$this->db->delete('tbl_post', array('ID' => $id));
		
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$rs = 0;
		} else {
			$this->db->trans_commit();
			$rs = 1;
		}
		
		$dt = array(
			'rs'=>$rs
		);
		echo json_encode($dt);
	}
}