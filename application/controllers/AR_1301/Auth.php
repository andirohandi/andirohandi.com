<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->helper('text');
		$this->load->model(array('m_admin'));

	}
    public function index() {
		
        $data['title'] = 'Login | ar.c';

        $this->load->view('vwLogin', $data);
    }
	
	function ver($id = ''){
		
		$username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($id) {
            echo "<center><h2 style='color:red'>Error 500. Forbiden Access !!</h2><center>";
        } else {
            if (isset($_POST['login'])) {
                $this->form_validation->set_rules('username', 'Username', 'trim|required');
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                if ($this->form_validation->run() == FALSE) {

                    $dt = array(
                        'result' => '2',
                        'username' => $username
                    );
                    $this->session->set_flashdata($dt);
                    $this->index();
                } else {
					$dt	= array(
						'USERNAME' => $username,
						'PASSWORD' => md5("hthxdhjklapJIOjh".md5($password)."asdfcnXhyYhgsdhGXChi"),
					);
					
					
					$cek = $this->m_admin->getSelectAuth($dt);
					
                    if (!$cek) {
                        $this->session->set_flashdata('error', 'Username dan Password anda tidak benar');
                        redirect('ar_1301/auth', 'refresh');
                    } else {
						
						$login 	= array(
							'ID' => $cek['ID'],
							'USERNAME' => $cek['USERNAME'],
							'logged_in' => true
						);
						
						$this->session->set_userdata($login);
						redirect('ar_1301/dashboard','refresh');
                    }
                }
            } else {
                redirect('ar_1301/auth', 'refresh');
            }
        }
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect('ar_1301/auth','refresh');
    }
}