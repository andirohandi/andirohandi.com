<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Saran extends CI_Controller {

    public function index() {
		
        $data['title'] = 'Saran | ar.c';
        $data['pg'] = 'saran';

        $this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/layout/vwContent');
        $this->load->view('back/layout/vwFooter');
    }
}