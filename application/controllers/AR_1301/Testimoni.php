<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller {

    public function index() {
		
        $data['title'] = 'Testimoni | ar.c';
        $data['pg'] = 'testimoni';

        $this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/layout/vwContent');
        $this->load->view('back/layout/vwFooter');
    }
}