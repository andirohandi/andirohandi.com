<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Komentar extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
			redirect('beranda');
		}
		$this->load->model('m_komentar');

	}
	
    public function index() {
		
        $data['title'] = 'Komentar | ar.c';
        $data['pg'] = 'komentar';

        $this->load->view('back/layout/vwNavbar', $data);
        $this->load->view('back/layout/vwSidebar');
        $this->load->view('back/komentar/vwKomentar');
        $this->load->view('back/layout/vwFooter');
    }
	
	public function read($pg=1)
	{
		
		$limit 		= $_POST['limit'];
		$status 	= $_POST['status'];
		$key 		= $_POST['key'];
		$offset 	= ($limit*$pg)-$limit;

		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_komentar->getSelectCount($status,$key);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_komentar->getSelect($status, $key, $limit, $offset);
		
		$this->load->view('back/komentar/vwList', $data);
		
	}
	
	public function delete()
	{
		$id = trim($_POST['i']);
		
		$this->m_komentar->getDelete($id);
		
		$dt = array(
			'rs' => 1
		);
		
		echo json_encode($dt);
	}
}