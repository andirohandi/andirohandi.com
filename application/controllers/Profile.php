<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model(array('m_kategori','m_post','m_komentar','m_testimoni'));
		$this->load->library('form_validation');
	}

	public function index() {
		
		$data['pg']		= "profile";
		$data['title']	= "Profile | ar.c";
		
		$this->load->view('front/layout/vwHeader',$data);
		$this->load->view('front/profile/vwIndex');
		$this->load->view('front/layout/vwBottomBar');
		$this->load->view('front/layout/vwSideBar');
		$this->load->view('front/layout/vwFooter');
	}
	
}
