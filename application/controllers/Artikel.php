<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->helper('text');
		$this->load->model(array('m_kategori','m_post','m_komentar'));

	}

	public function index() {
		
		$data['pg']		= "artikel";
		$data['title']	= "Artikel | ar.c";
		
		$this->load->view('front/layout/vwHeader',$data);
		$this->load->view('front/profile/vwIndex');
		$this->load->view('front/layout/vwBottomBar');
		$this->load->view('front/layout/vwSideBar');
		$this->load->view('front/layout/vwFooter');
	}
	
	function detail($id="",$judul="") {
		$id = decode($id);
		if($id == "" || $judul == "") {
			redirect('e/notfound','refresh');
		}else{
			$where	= array('a.ID'=>$id,'a.URL'=>$judul,'a.STATUS'=>1);
			$cek 	= $this->m_post->getSelectById($where);
			
			if(!$cek){
				redirect('e/notfound','refresh');
			}else{
				$data['pg']		= "artikel";
				$data['title']	= "Artikel | ar.c";
				$data['id_post']= encode($cek['ID']);
				$data['detail']	= $cek;
				$data['jmlah']	= $this->m_komentar->getCountKomentarByIdPost($id,1);
				$this->load->view('front/layout/vwHeader',$data);
				$this->load->view('front/artikel/vwDetail');
				$this->load->view('front/layout/vwBottomBar');
				$this->load->view('front/layout/vwSideBar');
				$this->load->view('front/layout/vwFooter');
			}
		}
	}
	
	function read($pg=1){
		$id_post= decode($_POST['id_post']);
		$limit 	= $_POST['limit'];
		$offset = ($limit*$pg)-$limit;
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_komentar->getCountKomentarByIdPost($id_post,1);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['komen']		= $this->m_komentar->getSelectKomentarByIdPost($id_post,1,$limit,$offset);
		
		$this->load->view('front/artikel/vwDetailListArtikel', $data);
	}
	
	function categori($id="",$url=""){
		if($id=="" || $url==""){
			redirect('e/notfound','refresh');
		}else{
			
			$where = array(
				'ID'	=> $id,
				'URL'	=> $url,
				'STATUS'	=> 1
			);
			
			$cek	= $this->m_kategori->getSelectById($where);
			
			if($cek == ""){
				redirect('e/notfound','refresh');
			}else{
				$data['pg']		= "artikel";
				$data['title']	= "Artikel ".$cek['NAMA_KTGR']." | ar.c";
				$data['artikel']= $cek;
				$this->load->view('front/layout/vwHeader',$data);
				$this->load->view('front/artikel/vwIndex');
				$this->load->view('front/layout/vwBottomBar');
				$this->load->view('front/layout/vwSideBar');
				$this->load->view('front/layout/vwFooter');
			}
		}
	}
	
	function categori_read($pg=1){
		$kategori	= $_POST['kategori'];
		$limit 		= $_POST['limit'];
		$key_text 	= $_POST['key'];
		$offset 	= ($limit*$pg)-$limit;

		$like = "a.JUDUL like '%$key_text%'";
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_post->count($like,$kategori);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_post->getSelect($like, $limit, $offset, $kategori);
		
		$this->load->view('front/artikel/vwList', $data);
	}
	
	function creat_komentar(){
		if(!isset($_POST['kirim_komentar'])){
			redirect('e/forbidden','refresh');
		}else{
			
			$this->form_validation->set_rules('nama','Nama','trim|required');
			$this->form_validation->set_rules('email','Email','trim|required');
			$this->form_validation->set_rules('komentar','Komentar','trim|required');
			
			$id_post= $this->input->post('id_post',true);
			$url	= $this->input->post('url',true);
				
			if ($this->form_validation->run() == FALSE) {
				$dt = array('result'=>2);
				$this->session->set_flashdata($dt);
				$this->detail($id_post,$url);
			}else{
				
				$nama	= $this->input->post('nama',true);
				$email	= $this->input->post('email',true);
				$komentar	= $this->input->post('komentar',true);
				
				$this->db->trans_begin();
				
				$data = array(
					'ID'		=> '',
					'NAMA'		=> $nama,
					'KOMENTAR'	=> $komentar,
					'EMAIL'		=> $email,
					'STATUS'	=> 1,
					'POST_ID'	=> decode($id_post)
				);
				
				$this->m_komentar->getInsert($data);
				
				if($this->db->trans_status() === false) {
					$this->db->trans_rollback();
					$dt = array('result'=>3);
					$this->session->set_flashdata($dt);
					redirect('artikel/detail/'.$id_post.'/'.$url,'refresh');
				}else{
					$limit = "";
					$offset = "";
					$get_data = $this->m_komentar->getSelectKomentarByIdPost(decode($id_post),1,$limit,$offset,$group="1");
					
					$post	= array(
						'NAMA_PENGIRIM'	=> $nama,
						'URL'			=> 'artikel/detail/'.$id_post.'/'.$url
					);
					
					if($get_data->num_rows() > 0){
						foreach($get_data->result() as $row){
							
							$komentar = array(
								'NAMA' => $row->NAMA,
								'EMAIL' => $row->EMAIL
							);
							if($row->EMAIL != $email){
								$this->send_email($post,$komentar);
							}else{
								
							}
						}
					}
					
					$this->db->trans_commit();
					
					$dt = array('result'=>1);
					$this->session->set_flashdata($dt);
					redirect('artikel/detail/'.$id_post.'/'.$url,'refresh');
				}
			}
		}
	}
	
	function send_email($post=array(),$komentar=array()){
		
		$msg	= "<b>Hi, ".$komentar['NAMA']."</b><br/>";
		$msg	.= $post['NAMA_PENGIRIM']." mengomentari artikel yang anda ikuti di <a href='".site_url('')."'>andirohandi.com</a>";
		$msg	.= "<br/>Berikut url lenkap nya. <a href='".site_url($post['URL'])."'>".site_url($post['URL'])."</a>";
		$msg	.= "<br/><br/>Salam,";
		$msg	.= "<br/>Admin andirohandi.com,";
		$this->load->library('email');
	    $this->email->from('andirohandi.android@gmail.com', 'andirohandi.com');
		$this->email->to($komentar['EMAIL']);
	    $this->email->subject("Notifikasi Komentar Artikel di Andirohandi.com");
	    $this->email->message($msg);
	   
	    if ($this->email->send())
	        return true;
	    else
	        return false;
	}
}
