<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->helper('text');
		$this->load->model(array('m_kategori','m_post','m_komentar'));

	}
	
	public function index() {
		
		$data['pg']		= "beranda";
		$data['title']	= "Beranda | ar.c";
		
		$this->load->view('front/layout/vwHeader',$data);
		$this->load->view('front/beranda/vwIndex');
		$this->load->view('front/layout/vwBottomBar');
		$this->load->view('front/layout/vwSideBar');
		$this->load->view('front/layout/vwFooter');
	}
	
	function read($pg=1) {		
		$kategori	= '';
		$limit 		= $_POST['limit'];
		$key_text 	= $_POST['key'];
		$offset 	= ($limit*$pg)-$limit;

		$like = "a.JUDUL like '%$key_text%'";
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_post->count($like,$kategori);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_post->getSelect($like, $limit, $offset, $kategori);
		
		$this->load->view('front/beranda/vwList', $data);
	}
	
}
