<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model(array('m_kategori','m_post','m_komentar','m_testimoni'));
		$this->load->library('form_validation');
	}

	public function index() {
		
		$data['pg']		= "testimoni";
		$data['title']	= "Testimoni | ar.c";
		
		$this->load->view('front/layout/vwHeader',$data);
		$this->load->view('front/testimoni/vwIndex');
		$this->load->view('front/layout/vwBottomBar');
		$this->load->view('front/layout/vwSideBar');
		$this->load->view('front/layout/vwFooter');
	}
	
	function create(){
		if(!isset($_POST['kirim_testimoni'])){
			redirect('e/forbidden','refresh');
		}else{
			
			$this->form_validation->set_rules('nama','Nama','trim|required');
			$this->form_validation->set_rules('email','Email','trim|required');
			$this->form_validation->set_rules('testimoni','Testimoni','trim|required');
			
			if ($this->form_validation->run() == FALSE) {
				$dt = array('result'=>2);
				$this->session->set_flashdata($dt);
				$this->index();
			}else{
				
				$nama	= $this->input->post('nama',true);
				$email	= $this->input->post('email',true);
				$testimoni	= $this->input->post('testimoni',true);
				
				$this->db->trans_begin();
				
				$data = array(
					'ID'		=> '',
					'NAMA'		=> $nama,
					'TESTIMONI'	=> $testimoni,
					'EMAIL'		=> $email
				);
				
				$this->m_testimoni->getInsert($data);
				
				if($this->db->trans_status() === false) {
					$this->db->trans_rollback();
					$dt = array('result'=>3);
					$this->session->set_flashdata($dt);
					redirect('testimoni','refresh');
				}else{
					$post = array(
						'NAMA'	=> $nama,
						'EMAIL'	=> $email
					);
					send_email($post);
					$this->db->trans_commit();
					$dt = array('result'=>1);
					$this->session->set_flashdata($dt);
					redirect('testimoni','refresh');
				}
			}
		}
	}
	
	function read($pg=1){
		$kategori	= '';
		$limit 		= $_POST['limit'];
		$offset 	= ($limit*$pg)-$limit;

		$like		= '';
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_testimoni->count($like,$kategori);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_testimoni->getSelect($like, $limit, $offset, $kategori);
		
		$this->load->view('front/testimoni/vwList', $data);
	}
	
	function send_email($post=array()){
		
		$msg	= "<b>Hi, ".$post['NAMA']."</b><br/>";
		$msg	.= "Terimakasih untuk testimoni yang anda tinggalkan di  <a href='".site_url('')."'>andirohandi.com</a>";
		$msg	.= "<br/><br/>Salam,";
		$msg	.= "<br/>Admin andirohandi.com,";
		$this->load->library('email');
	    $this->email->from('andirohandi.android@gmail.com', 'andirohandi.com');
		$this->email->to($post['EMAIL']);
	    $this->email->subject("Testimoni Untuk andirohandi.com");
	    $this->email->message($msg);
	   
	    if ($this->email->send())
	        return true;
	    else
	        return false;
	}
	
}
